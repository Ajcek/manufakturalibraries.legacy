﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model;
using Manufaktura.Controls.Model.Fonts;
using Manufaktura.Controls.Rendering;
using Manufaktura.Controls.Rendering.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manufaktura.Controls.Rendering.Implementations
{
    public class HtmlScoreRendererSettings : ScoreRendererSettings
    {
        public Dictionary<MusicFontStyles, HtmlFontInfo> Fonts { get; private set; }
        public HtmlRenderSurface RenderSurface { get; set; }
        public double Height { get; set; }
        public double MusicalFontShiftX { get; set; }
        public double MusicalFontShiftY { get; set; }

        public HtmlScoreRendererSettings()
        {
            Fonts = new Dictionary<MusicFontStyles, HtmlFontInfo>();
            RenderSurface = HtmlRenderSurface.Canvas;
            Height = 150;
			RenderingMode = ScoreRenderingModes.Panorama;
        }

        public enum HtmlRenderSurface
        {
            /// <summary>
            /// Indicates that the score will be rendered on HTML Canvas
            /// </summary>
            Canvas,
            /// <summary>
            /// Indicates that the score will be renderer in SVG tag
            /// </summary>
            Svg
        }
    }
}
