﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model.Fonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manufaktura.Controls.Rendering.Implementations
{
    public abstract class HtmlScoreRenderer<TCanvas> : ScoreRenderer<TCanvas>
    {
        public HtmlScoreRendererSettings TypedSettings { get { return Settings as HtmlScoreRendererSettings; } }
        public string ScoreElementName { get; set; }
        protected HtmlScoreRenderer(TCanvas canvas)
            : base(canvas)
        {
        }

        protected Primitives.Point TranslateTextLocation(Primitives.Point location, MusicFontStyles fontStyle)
        {
            double locationX = location.X + 3d + TypedSettings.MusicalFontShiftX;
            double locationY;
            switch (fontStyle)
            {
                case MusicFontStyles.MusicFont:
                    locationY = location.Y + 25d + TypedSettings.MusicalFontShiftY;
                    break;
                case MusicFontStyles.GraceNoteFont:
                    locationY = location.Y + 17.5d + TypedSettings.MusicalFontShiftY;
                    locationX += 0.7d;
                    break;
                default:
                    locationY = location.Y + 14d + TypedSettings.MusicalFontShiftY;
                    break;
            }
            return new Primitives.Point(locationX, locationY);
        }
    }
}
