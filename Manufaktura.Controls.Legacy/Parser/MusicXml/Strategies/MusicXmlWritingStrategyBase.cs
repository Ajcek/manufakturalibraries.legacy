﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace Manufaktura.Controls.Parser.MusicXml.Strategies
{
    public abstract class MusicXmlWritingStrategyBase
    {
        private static IEnumerable<MusicXmlWritingStrategyBase> _strategies;

        public abstract Type SymbolType { get; }
        public abstract string ElementName { get; }

        static MusicXmlWritingStrategyBase()
        {
            var strategyTypes = typeof(MusicXmlWritingStrategyBase).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(MusicXmlWritingStrategyBase)) && !t.IsAbstract);
            List<MusicXmlWritingStrategyBase> strategies = new List<MusicXmlWritingStrategyBase>();
            foreach (var type in strategyTypes)
            {
                strategies.Add(Activator.CreateInstance(type) as MusicXmlWritingStrategyBase);
            }
            _strategies = strategies.ToArray();
        }

        public void WriteElement(MusicalSymbol symbol, XElement parentElement)
        {
            var newElement = new XElement(XName.Get(ElementName));
            parentElement.Add(newElement);
            WriteElementInner(symbol, newElement);
        }

        protected abstract void WriteElementInner(MusicalSymbol symbol, XElement element);

        public static MusicXmlWritingStrategyBase GetProperStrategy(Type symbolType)
        {
            return _strategies.FirstOrDefault(s => s.SymbolType == symbolType);
        }
    }
}
