﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Extensions;
using Manufaktura.Controls.Formatting;
using Manufaktura.Controls.Model;
using Manufaktura.Controls.Parser.MusicXml;
using Manufaktura.Music.Xml;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Manufaktura.Controls.Parser
{
	/// <summary>
	/// Parser that parsers xml documents into scores.
	/// </summary>
	public class MusicXmlParser : ScoreParser<XDocument>
	{
		public override Score Parse(XDocument xmlDocument)  //TODO: Exception handling!
		{
			Score score = new Score();
			var firstSystem = new StaffSystem(score);
			score.Pages.Last().Systems.Add(firstSystem);
			MusicXmlParserState state = new MusicXmlParserState();

			foreach (XElement defaultSettingsNode in xmlDocument.Descendants().Where(d => d.Name == "defaults" || d.Name == "identification"))
			{
				foreach (XElement settingNode in defaultSettingsNode.Elements())
				{
					var dummyStaff = new Staff { Score = score };   //Nasty workaround
					MusicXmlParsingStrategy parsingStrategy = MusicXmlParsingStrategy.GetProperStrategy(settingNode);
					if (parsingStrategy != null) parsingStrategy.ParseElement(state, dummyStaff, settingNode);
				}
			}

			foreach (XElement partNode in xmlDocument.Descendants(XName.Get("part")))
			{
				state.CurrentSystemNo = 1;

				var partId = partNode.ParseAttribute("id");
				state.FirstLoop = true;
				Staff staff = new Staff() { MeasureAddingRule = Staff.MeasureAddingRuleEnum.AddMeasuresManually };
				score.Staves.Add(staff);
				var part = score.Parts.FirstOrDefault(p => p.PartId == partId) ?? new Part(staff) { PartId = partId };
				staff.Part = part;
				score.Parts.Add(part);
				foreach (XElement node in partNode.Elements())
				{
					MusicXmlParsingStrategy parsingStrategy = MusicXmlParsingStrategy.GetProperStrategy(node);
					if (parsingStrategy != null) parsingStrategy.ParseElement(state, staff, node);
				}
			}

			var partListNode = xmlDocument.Descendants(XName.Get("part-list"));
			PartGroup currentPartGroup = null;
			foreach (var partListElementNode in partListNode.Elements())
			{
				if (partListElementNode.Name == "part-group")
				{
					partListElementNode.IfAttribute("type").HasValue("start").Then(() =>
					{
						currentPartGroup = new PartGroup() { Number = score.PartGroups.Count + 1 };
						score.PartGroups.Add(currentPartGroup);
					});

					partListElementNode.IfAttribute("type").HasValue("stop").Then(() => currentPartGroup = null);
					if (currentPartGroup != null)
					{
						partListElementNode.IfElement("group-barline").HasValue<GroupBarlineType>(d =>
						{
							d.Add("Yes", GroupBarlineType.Enabled);
							d.Add("No", GroupBarlineType.Disabled);
							d.Add("Mensurstrich", GroupBarlineType.Mensurstrich);
							return d;
						}).Then(r => currentPartGroup.GroupBarline = r);
					}
				}
				if (partListElementNode.Name == "score-part" && currentPartGroup != null)
				{
					var scorePartId = partListElementNode.IfAttribute("id").HasAnyValue().ThenReturnResult();
					var matchingScorePart = score.Parts.FirstOrDefault(p => p.PartId == scorePartId);
					if (matchingScorePart != null) matchingScorePart.Group = currentPartGroup;
				}
			}

			//Sibelius hack:
			if (score.Encoding?.Software?.Any(s => s.Contains("Sibelius")) ?? false)
			{
				new DefaultScoreFormatter().Format(score);
			}

			foreach (var staff in score.Staves) staff.MeasureAddingRule = Staff.MeasureAddingRuleEnum.AddMeasureOnInsertingBarline;
			return score;
		}

		public override XDocument ParseBack(Score score)
		{
			throw new NotImplementedException("This parser does not support saving to MusicXml.");
		}
	}
}