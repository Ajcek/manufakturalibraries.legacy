﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model;
using Manufaktura.Music.Model;
using System;

namespace Manufaktura.Controls.Primitives
{
	/// <summary>
	/// Represents a 2D point.
	/// </summary>
	public struct Point
	{
		public Point(double x, double y) : this()
		{
			this.X = x;
			this.Y = y;
		}

		public double X { get; set; }

		public double Y { get; set; }

		public static double BeamAngle(Point beamStart, Point beamEnd)
		{
			return UsefulMath.BeamAngle(beamStart.X, beamStart.Y, beamEnd.X, beamEnd.Y);
		}

		public override string ToString()
		{
			return $"[{X}, {Y}]";
		}

		public Point Translate(ScorePage page)
		{
			return new Point(this.X + (page.MarginLeft ?? 0), this.Y + (page.MarginTop ?? 0));
		}

		public Point Translate(double dx, double dy)
		{
			return new Point(X + dx, Y + dy);
		}

		public Point TranslateByAngle(double angle, double length)
		{
			var dy = Math.Sin(angle) * length;
			var dx = Math.Cos(angle) * length;
			return new Point(X + dx, Y + dy);
		}

		public Point TranslateHorizontallyAndMaintainAngle(double angle, double dx)
		{
			var dy = Math.Tan(angle) * dx;
			return new Point(X + dx, Y + dy);
		}
	}
}