﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model.Fonts;
using Manufaktura.Controls.Primitives;
using Manufaktura.Music.Model;

namespace Manufaktura.Controls.Model
{
	/// <summary>
	/// Base class that represents note or rest.
	/// </summary>
	public abstract class NoteOrRest : MusicalSymbol, IHasDuration, ICanBeElementOfTuplet, IHasCustomXPosition, IRenderedAsTextBlock
	{
		protected bool hasFermataSign = false;
		protected Point location = new Point();
		protected string musicalCharacter;
		protected TupletType tuplet = TupletType.None;
		private RhythmicDuration duration;
		private IMusicFont musicFont = new PolihymniaFont();

		protected NoteOrRest()
		{
			Voice = 1;
		}

		/// <summary>
		/// Duration without dots.
		/// </summary>
		public RhythmicDuration BaseDuration
		{
			get
			{
				return Duration.WithoutDots;
			}
		}

		public double? DefaultXPosition { get; set; }

		public RhythmicDuration Duration { get { return duration; } set { duration = value; OnPropertyChanged(() => Duration); } }

		/// <summary>
		/// Indicates if note has fermata sign
		/// </summary>
		public bool HasFermataSign { get { return hasFermataSign; } set { hasFermataSign = value; OnPropertyChanged(() => HasFermataSign); } }

		public string MusicalCharacter
		{
			get { return musicalCharacter; }
		}

		/// <summary>
		/// Music font used to draw the note
		/// </summary>
		public IMusicFont MusicFont { get { return musicFont; } set { musicFont = value; OnPropertyChanged(() => MusicFont); } }

		public int NumberOfDots
		{
			get
			{
				return Duration.Dots;
			}
			set
			{
				Duration = new RhythmicDuration(BaseDuration.Denominator, value);
			}
		}

		public Point TextBlockLocation { get { return location; } set { location = value; } }

		public TupletType Tuplet { get { return tuplet; } set { tuplet = value; } }

		public double? TupletWeightOverride { get; set; }
		public VerticalPlacement? TupletPlacement { get; set; }

		/// <summary>
		/// Voice number.
		/// </summary>
		public int Voice { get; set; }

		/// <summary>
		/// Creates a Note or a Rest from RhythmicUnit.
		/// </summary>
		/// <param name="unit"></param>
		/// <returns></returns>
		public static NoteOrRest FromRhythmicUnit(RhythmicUnit unit)
		{
			return unit.IsRest ? (NoteOrRest)new Rest(unit.Duration) : new Note(unit.Duration);
		}
	}
}