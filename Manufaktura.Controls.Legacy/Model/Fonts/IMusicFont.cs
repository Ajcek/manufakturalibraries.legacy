﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace Manufaktura.Controls.Model.Fonts
{
    /// <summary>
    /// Character mapping definition for music font.
    /// </summary>
    public interface IMusicFont
    {
        string BlackNoteHead { get; }

        string CClef { get; }

        string CommonTime { get; }

        string CutTime { get; }

        string D32ndRest { get; }

        string Dot { get; }

        string DoubleFlat { get; }

        string DoubleSharp { get; }

        string EighthNote { get; }

        string EighthRest { get; }

        string FClef { get; }

        string FermataDown { get; }

        string FermataUp { get; }

        string Flat { get; }

        string GClef { get; }

        string HalfNote { get; }

        string HalfRest { get; }

        string LeftBracket { get; }

        string LeftSquareBracket { get; }

        string Mordent { get; }

        string MordentShort { get; }

        string Natural { get; }

        string NoteFlag128th { get; }

        string NoteFlag128thRev { get; }

        string NoteFlag32nd { get; }

        string NoteFlag32ndRev { get; }

        string NoteFlag64th { get; }

        string NoteFlag64thRev { get; }

        string NoteFlagEighth { get; }

        string NoteFlagEighthRev { get; }

        string NoteFlagSixteenth { get; }

        string NoteFlagSixteenthRev { get; }

        string QuarterNote { get; }

        string QuarterRest { get; }

        string RepeatBackward { get; }

        string RepeatForward { get; }

        string RightBracket { get; }

        string Sharp { get; }

        string SixteenthNote { get; }

        string SixteenthRest { get; }

        string Staff4Lines { get; }

        string Staff5Lines { get; }

        string Trill { get; }

        string WhiteNoteHead { get; }

        string WholeNote { get; }

        string WholeRest { get; }
    }
}