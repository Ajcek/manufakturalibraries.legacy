﻿/*
 * Copyright 2018 Manufaktura Programów Jacek Salamon http://musicengravingcontrols.com/
 * MIT LICENCE
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using Manufaktura.Controls.Model.Fonts;
using Manufaktura.Music.Model;
using Manufaktura.Music.Model.MajorAndMinor;
using System;

namespace Manufaktura.Controls.Model
{
	/// <summary>
	/// Represents a key signature.
	/// </summary>
	public class Key : MusicalSymbol, IRenderedAsTextBlock
	{
		private int fifths;
		private string musicalCharacter;
		private IMusicFont musicFont = new PolihymniaFont();

		/// <summary>
		/// Initializes a new instance of key
		/// </summary>
		/// <param name="numberOfFifths"></param>
		public Key(int numberOfFifths)
		{
			fifths = numberOfFifths;
			if (fifths > 0)
				musicalCharacter = MusicFont.Sharp;
			else if (fifths < 0)
				musicalCharacter = MusicFont.Flat;
			else
				musicalCharacter = " ";
		}

		public int Fifths { get { return fifths; } }

		public string MusicalCharacter
		{
			get { return musicalCharacter; }
		}

		public IMusicFont MusicFont { get { return musicFont; } set { musicFont = value; OnPropertyChanged(() => MusicFont); } }

		public Primitives.Point TextBlockLocation
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Creates a new instance of Key from given midi pitch.
		/// </summary>
		/// <param name="midiPitch">Midi pitch</param>
		/// <param name="flags">Flags</param>
		/// <returns>Key</returns>
		public static Key FromMidiPitch(int midiPitch, MajorAndMinorScaleFlags flags)
		{
			return new Key(CircleOfFifths.CalculateFifths(midiPitch, flags));
		}

		/// <summary>
		/// Creates a new instance of Key from given Scale
		/// </summary>
		/// <param name="scale"></param>
		/// <returns></returns>
		public static Key FromScale(MajorOrMinorScale scale)
		{
			return new Key(scale.Fifths);
		}

		/// <summary>
		/// Creates a new instance of Key from given step.
		/// </summary>
		/// <param name="step"></param>
		/// <param name="flags"></param>
		/// <returns></returns>
		public static Key FromTonic(Step step, MajorAndMinorScaleFlags flags)
		{
			return new Key(CircleOfFifths.CalculateFifths(step, flags));
		}

		/// <summary>
		/// Returns the alteration of specific step.
		/// </summary>
		/// <param name="step"></param>
		/// <returns></returns>
		public int StepToAlter(string step)
		{
			return CircleOfFifths.GetAlterOfStepFromNumberOfFifths(step, Fifths);
		}

		public override string ToString()
		{
			return string.Format("{0} with {1} generator intervals.", base.ToString(), Fifths);
		}
	}
}